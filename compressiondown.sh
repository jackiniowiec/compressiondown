#!/bin/sh

ZIP="zip"
TAR="tar"

while :
do
    ZIPPID=`pgrep ${ZIP}`
    TARPID=`pgrep ${TAR}`

    if [ "${ZIPPID:-null}" = null ]; then
        echo "ZIP not running"

    else
        echo "ZIP - renice" && renice -n -20 $ZIPPID && echo "ZIP - ionice" && ionice -c3 -p $ZIPPID
        fi

    if [ "${TARPID:-null}" = null ]; then
        echo "TAR not running"

    else
        echo "TAR - renice" && renice -n -20 $TARPID && echo "TAR - ionice" && ionice -c3 -p $TARPID
        fi
    sleep 10
done
